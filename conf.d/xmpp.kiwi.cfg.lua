VirtualHost "inblack.biz"

    enabled = true
    anonymous_login = false
    allow_registration = false -- Disable account creation by default, for security
    min_seconds_between_registrations = 300 -- time between registration attempts
    c2s_require_encryption = true -- Force clients to use encrypted connections?
    s2s_require_encryption = true -- Force servers to use encrypted connections?
    s2s_secure_auth = true -- Force certificate authentication for server-to-server connections?
    welcome_message = "Welcome to $host!"

    ssl = {
        key = "/etc/prosody/certs/inblack.biz.key";
        certificate = "/etc/prosody/certs/inblack.biz.crt";
        options = { "no_sslv2", "no_sslv3", "no_ticket", "no_compression" };
        ciphers = "ECDH:DH:!CAMEL!LIA128:!3DES:MD5:!RC4:!aNULL:!NULL:!EXPORT:!LOW:!MEDIUM";
        dhparam = "/etc/prosody/certs/dh-4096.pem";
    }

    Component "muc.inblack.biz" "muc"
	name = "inblack.biz MUC"
	restrict_room_creation = "local"
    modules_enabled = { "vcard_muc", "muc_limits" };
    -- modules_enabled = { "vcard_muc", "muc_mam", "muc_limits" };
	muc_event_rate = 1
	muc_burst_factor = 6
	muc_max_nick_length = 20
