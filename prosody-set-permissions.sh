#!/bin/bash
# This script ensures that files and directories are chowned properly

PROSODY_CERTS="/etc/prosody/certs"
PROSODY_LIB="/var/lib/prosody"
PROSODY="/etc/prosody"


# Set permsission and reload service
chown -R prosody:prosody "${PROSODY}"
chown -R prosody:prosody "${PROSODY_LIB}"
chmod -R 700 "${PROSODY_CERTS}" # have to check if these are correct permissions @todo
prosodyctl reload

# Userdir
#chown -R user:user /home/user
