daemonize = false;
-- use_libevent = true;
network_backend = "epoll"
pidfile = "/var/run/prosody/prosody.pid"
plugin_paths = { "/var/lib/prosody/modules" }
c2s_ports = { "5222" }
-- legacy_ssl_ports = { "5223" }
default_storage = "internal"
authentication = "internal_hashed"

-- Example: admins = { "user1@example.com", "user2@example.net" }

admins = {"admin@telegramma.me"}

-- interfaces = { "" }

modules_enabled = {

	-- Core Enabled Modules --

	"roster"; -- Allow users to have a roster. Recommended ;)
	"saslauth"; -- Authentication for clients and servers. Recommended if you want to log in.
	"tls"; -- Add support for secure TLS on c2s/s2s connections
	"dialback"; -- s2s dialback support
	"disco"; -- Service discovery
	"posix"; -- POSIX functionality, sends server to background, enables syslog, etc.
	"private"; -- Private XML storage (for room bookmarks, etc.)
	"vcard4"; -- Add support for the new vcard format
	"vcard_legacy"; -- Support old vcard format for legacy clients
	"version"; -- Replies to server version requests
	"uptime"; -- Report how long server has been running
	"time"; -- Let others know the time here on this server
	"ping"; -- Replies to XMPP pings with pongs
	"pep"; -- Enables users to publish their mood, activity, playing music and more
	"register"; -- Allow users to register on this server using a client and change passwords
	"admin_adhoc"; -- Allows administration via an XMPP client that supports ad-hoc commands
	"announce"; -- Send announcement to all online users
	"bosh"; -- Enable BOSH clients, aka "Jabber over HTTP"
  	"admin_telnet"; -- Opens telnet console interface on localhost port 5582
	"welcome"; -- Welcome users who register accounts
	"blocklist"; -- New module replacing mod_privacy
	"carbons"; -- Officially included in Prosody now
	"proxy65"; -- Proxy for clients behind NAT or firewalls
	"watchregistrations"; -- Alert admins of registrations

	--- Downloaded & Enabled Modules ---

	-- Misc --

	"reload_modules";		
	"default_vcard";
	-- "cloud_notify";
	-- "server_contact_info";
	"lastlog";
	"bookmarks";

	-- Security --

	"filter_chatstates";
	"block_registrations";
	"limits";
	"limit_auth";

	-- Optimzation --

	"smacks";
	"csi";
	"csi_battery_saver";
	"log_slow_events";
	-- "mam";
	};
	
	modules_disabled = {
	
	-- Disabled Modules --

	"groups"; -- Shared roster support
	-- "watchregistrations"; -- Alert admins of registrations
	"motd"; -- Send a message to users when they log in
	"legacyauth"; -- Legacy authentication. Only used by some old clients and bots.
  	"http_files"; -- Serve static files from a directory over HTTP
	-- "offline"; -- Offline messages
	"proxy65"; -- Proxy for clients behind NAT or firewalls
	"admin_telnet"; -- Opens telnet console interface on localhost port 5582
	"bosh"; -- Enable BOSH clients, aka "Jabber over HTTP"

	};

	-- mod_watchregistrations --
	
	registration_watchers = { "admin@telegramma.me" } -- mod_watchregistrations will use this list of users instead of the admin list
	registration_notification = "User $username just registered on $host"

	-- mod_server_contact_info --

	-- contact_info = {
	-- abuse = { "https://xmpp.is/contact/" };
	-- admin = { "https://xmpp.is/contact/" };
	-- feedback = { "https://xmpp.is/contact/" };
	-- support = { "https://xmpp.is/contact/" };
	-- };

	-- Log Config --

	log = {
	
	-- uncomment to turn on logging --

	-- info = "/var/log/prosody/prosody.info";
	-- warn = "/var/log/prosody/prosody.warn";
	-- error = "/var/log/prosody/prosody.err";

	-- logging disabled --

	info = "/dev/null";
	warn = "/dev/null";
	error = "/dev/null";
	debug = "/dev/null";
	
	}

	-- mod_limits --

	limits = {

	c2s = {
	rate = "5kb/s";
	burst = "10s";
	};

	s2sin = {
	rate = "5kb/s";
	burst = "10s";
	};

	s2sout = {
	rate = "5kb/s";
	burst = "10s";
	};

	}

-- mod_log_slow_events --
log_slow_events_threshold = 1

-- mod_limit_auth --
limit_auth_period = 30
limit_auth_max = 5

-- mod_mam --
-- default_archive_policy = false -- You can also configure messages to be stored in-memory only. For more archiving options, see https://prosody.im/doc/modules/mod_mam
-- archive_cleanup_interval = 60 * 2
-- archive_expires_after = "1d"
-- max_archive_query_results = 20;
-- mam_smart_enable = true

-- mod_smacks --
smacks_hibernation_time = 300
smacks_enabled_s2s = false
smacks_max_unacked_stanzas = 0
smacks_max_ack_delay = 60
smacks_max_hibernated_sessions = 10
smacks_max_old_sessions = 10

-- mod_block_registrations --
block_registrations_users = { "administrator", "admin", "hostmaster", "postmaster", "webmaster", "root", "xmpp" }
block_registrations_require = "^[a-zA-Z0-9_.-]+$" -- Allow only simple ASCII characters in usernames

certificates = "certs" -- Location of directory to find certificates in (relative to main config file)

Include "conf.d/*.cfg.lua"
