# Prosody XMPP server installation

## Update server and install required packages

```
apt update && apt-get upgrade -y
apt install wget curl mc fail2ban vim htop lua-zlib socat -y
```

## Prepare server for installation


## Install Prosody

Add repo to sources.list

```
echo deb http://packages.prosody.im/debian bionic main | tee -a /etc/apt/sources.list
```

Add repo key

```
wget --no-check-certificate https://prosody.im/files/prosody-debian-packages.key
apt-key add prosody-debian-packages.key
```

Update and install prosody 

```
apt update
apt install prosody 
```
Create Letsencrypt certificates and install acme.sh to keep everything up to date 

Install acme.sh

```
curl https://get.acme.sh | sh
```

Issue a certificate  

```
./acme.sh --issue --standalone -d skyclub.nz --fullchain-file /etc/prosody/certs/skyclub.nz.crt --key-file /etc/prosody/certs/skyclub.nz.key
acme.sh --reloadcmd "service prosody restart" --install-cronjob
```

Generate a DH key

```
openssl dhparam -out /etc/prosody/certs/dh-4096.pem 4096
```

## Post installation work

### Set file and folder permissions

```bash
chown -R prosody:prosody /etc/prosody
chmod -R 0400 /etc/prosody/certs/*.*
chown -R prosody:prosody /var/lib/prosody
chmod -R 750 /var/lib/prosody/*.*
```

### Set up domain records

Add correct SRV or TXT records with your domain registrar 

```
_xmpp-client._tcp.conference.domain.com 20 0 5222
_xmpp-client._tcp.domain.com 20 0 5222
_xmpp-server._tcp.conference.domain.com 20 0 5269
_xmpp-server._tcp.domain.com 20 0 5269
```

## Other things to note

Prosody main configuration file

```
/etc/prosody/prosody.cfg.lua
```

Prosody log storage

```
/var/log/prosody
```

Prosody data storage

```
/var/lib/prosody
```

How to add a user

```
prosodyctl adduser nick@domain
```

Restar a server

```
prosodyctl restart
```

