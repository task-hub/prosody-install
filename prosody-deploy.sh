#!/bin/bash
# Deploys Prosody on a new server

#GIT_DIR="/home/user/git"
PROSODY_DATA_DIR="/var/lib/prosody"

echo "Please enter domain or subdomain name:"
read prosody_domain &&

echo "================================"

echo "Installing miscellaneous packages that I like or need :^)"
apt update && apt-get upgrade -y
apt update && apt install -y screen rsync iftop iotop dstat nload gnupg netcat git mercurial wget curl mc fail2ban vim htop socat

echo "================================"

echo "Adding Prosody Repository Key" 
echo deb http://packages.prosody.im/debian bionic main | tee -a /etc/apt/sources.list
wget --no-check-certificate https://prosody.im/files/prosody-debian-packages.key -O- | apt-key add -

echo "================================"

echo "Running apt update"
apt update

echo "================================"

echo "Installing Prosody"
apt update && apt install -y prosody lua-event lua-sec lua-zlib

echo "================================"

echo "Making directories"
mkdir -p /etc/prosody/certs
mkdir -p /etc/prosody/conf.d

echo "================================"

echo "Pulling configs and modules"

# Prosody configs & scripts
# git clone https://github.com/crypto-world/xmpp.is "${GIT_DIR}"/xmpp.is @todo

# Official Prosody modules
hg clone https://hg.prosody.im/prosody-modules/ "${PROSODY_DATA_DIR}"/modules


echo "================================"

echo "Generating DH key, this is a long procedure"
openssl dhparam -out /etc/prosody/certs/dh-4096.pem 4096

echo "================================"

echo "Setting file and directory permissions"
chmod +x ./prosody-set-permissions.sh
./prosody-set-permissions.sh
